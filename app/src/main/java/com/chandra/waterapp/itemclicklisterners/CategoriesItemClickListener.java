package com.chandra.waterapp.itemclicklisterners;

import android.view.View;

public interface CategoriesItemClickListener
{
    void onItemClick(View v, int pos);
}
