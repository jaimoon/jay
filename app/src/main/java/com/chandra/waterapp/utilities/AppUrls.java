package com.chandra.waterapp.utilities;

public class AppUrls {

    public static String BASE_URL = "http://localhost/waterApp/";
    public static String IMAGE_URL = "http://localhost/waterApp/images/products/280x280/";

    public static String LOGIN = "login";
    public static String REGISTER = "register";
    public static String FORGOTPASSWORD = "forgotpwd";
    public static String RESENDOTP = "resend_otp";
    public static String VERIFYACCOUNT = "verifyotp";
    public static String CHANGEPASSWORD = "changePassword";
    public static String FORGOTCHANGEPASSWORD = "forgotChangePassword";

    /*Main*/
    public static String CATEGORIES = "category";

}

