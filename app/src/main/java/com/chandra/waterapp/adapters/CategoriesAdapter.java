package com.chandra.waterapp.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chandra.waterapp.R;
import com.chandra.waterapp.activities.ListOfShops;
import com.chandra.waterapp.fragments.HomeFragment;
import com.chandra.waterapp.holders.CategoriesHolder;
import com.chandra.waterapp.itemclicklisterners.CategoriesItemClickListener;
import com.chandra.waterapp.models.CategoriesModel;
import com.chandra.waterapp.utilities.UserSessionManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesHolder> {

    private ArrayList<CategoriesModel> categoriesModels;
    HomeFragment context;
    LayoutInflater li;
    int resource;
    private boolean checkInternet;
    UserSessionManager session;
    ProgressDialog progressDialog;
    Typeface typeface;

    public CategoriesAdapter(ArrayList<CategoriesModel> categoriesModels, HomeFragment context, int resource) {
        this.categoriesModels = categoriesModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.fonttype_two));

    }

    @NonNull
    @Override
    public CategoriesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutSeedCategory = li.inflate(resource, null);
        return new CategoriesHolder(layoutSeedCategory);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoriesHolder holder, final int position) {

        progressDialog = new ProgressDialog(context.getActivity());
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        final String get_id = categoriesModels.get(position).getId();
        String str = categoriesModels.get(position).getName();
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.category_name.setText(converted_string);
        holder.category_name.setTypeface(typeface);

        Log.d("VALUES:", str + "" + converted_string);

        /*if (str.equals("Fish")) {

            Picasso.with(context.getActivity())
                    .load(R.drawable.fish_image)
                    .into(holder.category_img);
        }

        if (str.equals("Prawn")) {

            Picasso.with(context.getActivity())
                    .load(R.drawable.prawn_image)
                    .into(holder.category_img);
        } else {
            Picasso.with(context.getActivity())
                    .load(R.drawable.fish_image)
                    .into(holder.category_img);
        }*/

        holder.setItemClickListener(new CategoriesItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                if (get_id.equals("1")) {
                    Intent intent = new Intent(context.getActivity(), ListOfShops.class);
                    intent.putExtra("cat_id", "1");
                    context.startActivity(intent);
                }
                if (get_id.equals("2")) {
                    Intent intent = new Intent(context.getActivity(), ListOfShops.class);
                    intent.putExtra("cat_id", "2");
                    context.startActivity(intent);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.categoriesModels.size();
    }

}
