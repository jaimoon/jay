package com.chandra.waterapp;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chandra.waterapp.activities.ProfilePage;
import com.chandra.waterapp.fragments.HomeFragment;
import com.chandra.waterapp.utilities.CustomTypefaceSpan;

public class MainActivity extends AppCompatActivity {

    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    ImageView profileImage;
    TextView txtViewName,txtViewEmail,txtViewPhone,toolbar_title;
    Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        typeface = Typeface.createFromAsset(this.getAssets(), "latoregular.ttf");

        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(typeface);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.navigationview) ;

        Menu m = mNavigationView.getMenu();
        for (int i=0;i<m.size();i++) {
            MenuItem mi = m.getItem(i);

            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }
            applyFontToMenuItem(mi);
        }

        mNavigationView.setItemIconTintList(null);
        View navigationheadderView = mNavigationView.getHeaderView(0);
        profileImage = (ImageView) navigationheadderView.findViewById(R.id.profileImage);
        txtViewName = (TextView) navigationheadderView.findViewById(R.id.txtViewName);
        txtViewName.setTypeface(typeface);
        txtViewEmail = (TextView) navigationheadderView.findViewById(R.id.txtViewEmail);
        txtViewEmail.setTypeface(typeface);
        txtViewPhone = (TextView) navigationheadderView.findViewById(R.id.txtViewPhone);
        txtViewPhone.setTypeface(typeface);

        /*Picasso.with(getApplicationContext())
                .load(imageUrl)
                .placeholder(R.drawable.profile_user)
                .fit()
                .into(profileImage);*/

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView,new HomeFragment()).commit();

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                mDrawerLayout.closeDrawers();

                if (menuItem.getItemId() == R.id.home) {

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                }
                if (menuItem.getItemId() == R.id.profile) {

                    Intent intent = new Intent(getApplicationContext(), ProfilePage.class);
                    startActivity(intent);

                }

                if (menuItem.getItemId() == R.id.share) {

                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Sample");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/water");
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));

                }

                /*if (menuItem.getItemId() == R.id.logout) {

                    if(loginType.equals("google")) {
                        Intent intent = new Intent(getApplicationContext(), LoginPage.class);
                        userSessionManager.logoutUser();

                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {

                                    }
                                });
                        startActivity(intent);
                    }

                    if(loginType.equals("facebook")) {
                        Intent intent = new Intent(getApplicationContext(), LoginPage.class);


                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();

                        userSessionManager.logoutUser();
                        startActivity(intent);
                    }

                    if(loginType.equals("famboat")) {
                        Intent intent = new Intent(getApplicationContext(), LoginPage.class);

                        userSessionManager.logoutUser();
                        startActivity(intent);
                    }
                }*/

                return false;
            }
        });

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout, toolbar,R.string.app_name, R.string.app_name);

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();

    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "latoregular.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }
}
