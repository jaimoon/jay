package com.chandra.waterapp.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chandra.waterapp.R;
import com.chandra.waterapp.itemclicklisterners.CategoriesItemClickListener;

public class CategoriesHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView category_name;
    CategoriesItemClickListener areaItemClickListener;

    public CategoriesHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        category_name = itemView.findViewById(R.id.category_name);
    }

    @Override
    public void onClick(View view) {
        this.areaItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(CategoriesItemClickListener ic)
    {
        this.areaItemClickListener =ic;
    }
}
