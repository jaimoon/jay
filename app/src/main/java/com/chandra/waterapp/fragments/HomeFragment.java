package com.chandra.waterapp.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chandra.waterapp.R;
import com.chandra.waterapp.activities.ListOfShops;
import com.chandra.waterapp.adapters.CategoriesAdapter;
import com.chandra.waterapp.models.CategoriesModel;
import com.chandra.waterapp.utilities.AppUrls;
import com.chandra.waterapp.utilities.NetworkChecking;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class HomeFragment extends Fragment implements View.OnClickListener {

    //ImageView cans_img,tankers_img;
    /*RadioGroup radio_type;
    RadioButton radio_cans,radio_tankers;
    TextView choose_txt;
    EditText count_edt;*/

    RecyclerView categories;
    ArrayList<CategoriesModel> categoriesModels = new ArrayList<CategoriesModel>();
    CategoriesAdapter adapter;
    Button go_btn;
    ProgressDialog progressDialog;
    boolean checkInternet;
    Typeface typeface;
    View rootView;

    public HomeFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_home, container, false);

        typeface = Typeface.createFromAsset(getActivity().getAssets(), "latoregular.ttf");

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        /*radio_type = (RadioGroup) rootView.findViewById(R.id.radio_type);

        radio_cans = (RadioButton) rootView.findViewById(R.id.radio_cans);
        radio_cans.setTypeface(typeface);
        radio_cans.setOnClickListener(this);

        radio_tankers = (RadioButton) rootView.findViewById(R.id.radio_tankers);
        radio_tankers.setTypeface(typeface);
        radio_tankers.setOnClickListener(this);

        choose_txt = (TextView) rootView.findViewById(R.id.choose_txt);
        choose_txt.setTypeface(typeface);

        count_edt = (EditText) rootView.findViewById(R.id.count_edt);
        count_edt.setTypeface(typeface);*/

        categories = (RecyclerView) rootView.findViewById(R.id.categories);
        adapter = new CategoriesAdapter(categoriesModels, HomeFragment.this, R.layout.row_categories);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 1);
        categories.setNestedScrollingEnabled(false);
        categories.setLayoutManager(layoutManager);

        go_btn = (Button) rootView.findViewById(R.id.go_btn);
        go_btn.setTypeface(typeface);
        go_btn.setOnClickListener(this);

        return rootView;
    }

    private void getMainCategory(final int default_page_number) {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {
            Log.d("MAINCATEGURL", AppUrls.BASE_URL + AppUrls.CATEGORIES);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.CATEGORIES,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("MAINCATEGRESP", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        CategoriesModel gbm = new CategoriesModel();
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        gbm.setId(jsonObject1.getString("id"));
                                        gbm.setName(jsonObject1.getString("name"));
                                        gbm.setCreated_date(jsonObject1.getString("created_date"));
                                        gbm.setUpdated_date(jsonObject1.getString("updated_date"));
                                        categoriesModels.add(gbm);
                                    }
                                    categories.setAdapter(adapter);

                                }
                                if (responceCode.equals("12786")) {
                                    Toast.makeText(getActivity(), "No Data Found..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("11786")) {
                                    Toast.makeText(getActivity(), "All fields are required..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("10786")) {
                                    Toast.makeText(getActivity(), "Invalid Token.!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View v) {

        if (v == go_btn){

            Intent intent = new Intent(getActivity(), ListOfShops.class);
            startActivity(intent);
        }

    }
}
