package com.chandra.waterapp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chandra.waterapp.MainActivity;
import com.chandra.waterapp.R;
import com.chandra.waterapp.utilities.AppUrls;
import com.chandra.waterapp.utilities.NetworkChecking;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class LoginPage extends AppCompatActivity implements View.OnClickListener {

    ImageView login_logo;
    TextView login_txt,login_forgotpassword,login_donthaveaccount,registerhere;
    TextInputLayout username,password;
    EditText username_txt;
    TextInputEditText password_txt;
    Button login_cancel,login_submit;
    Typeface typeface;
    boolean checkInternet;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        typeface = Typeface.createFromAsset(this.getAssets(), "latoregular.ttf");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        login_logo = findViewById(R.id.login_logo);

        login_txt = findViewById(R.id.login_txt);
        login_txt.setTypeface(typeface);
        login_forgotpassword = findViewById(R.id.login_forgotpassword);
        login_forgotpassword.setTypeface(typeface);
        login_forgotpassword.setOnClickListener(this);
        login_donthaveaccount = findViewById(R.id.login_donthaveaccount);
        login_donthaveaccount.setTypeface(typeface);
        registerhere = findViewById(R.id.registerhere);
        registerhere.setTypeface(typeface);
        registerhere.setOnClickListener(this);

        username_txt = findViewById(R.id.username_txt);
        username_txt.setTypeface(typeface);

        username = findViewById(R.id.username);
        username.setTypeface(typeface);
        password = findViewById(R.id.password);
        password.setTypeface(typeface);

        password_txt = findViewById(R.id.password_txt);
        password_txt.setTypeface(typeface);

        login_cancel = findViewById(R.id.login_cancel);
        login_cancel.setTypeface(typeface);
        login_cancel.setOnClickListener(this);

        login_submit = findViewById(R.id.login_submit);
        login_submit.setTypeface(typeface);
        login_submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == login_cancel){

            finish();

        }

        if (v == login_submit){

            if (validate()) {
                checkInternet = NetworkChecking.isConnected(this);
                if (checkInternet) {

                    if (username_txt.getText().length() > 0) {
                        if (password_txt.getText().length() > 0) {

                        }
                    }

                    final String email = username_txt.getText().toString().trim();
                    final String psw = password_txt.getText().toString().trim();
                    progressDialog.show();
                    PackageInfo pInfo = null;
                    String version = null;
                    try {
                        pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                        version = pInfo.versionName;
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    final String finalVersion = version;

                    Log.d("LOGINURL:", AppUrls.BASE_URL + AppUrls.LOGIN);
                    StringRequest strReqLogin = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOGIN, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("LOGIN", response);
                                String editSuccessResponceCode = jsonObject.getString("status");

                                if (editSuccessResponceCode.equalsIgnoreCase("10100")) {

                                    progressDialog.dismiss();
                                    JSONObject jdata = jsonObject.getJSONObject("data");
                                    String jwt = jdata.getString("jwt");
                                    Log.d("JWT", jwt);
                                    String[] parts = jwt.split("\\.");
                                    byte[] dataDec = Base64.decode(parts[1], Base64.DEFAULT);
                                    String decodedString = "";
                                    try {

                                        decodedString = new String(dataDec, "UTF-8");
                                        Log.d("TOKENSFSF", decodedString);
                                        JSONObject jsonObject2 = new JSONObject(decodedString);
                                        Log.d("JSONDATAsfa", jsonObject2.toString());
                                        JSONObject jsonObject3 = jsonObject2.getJSONObject("data");

                                        String user_id = jsonObject3.getString("user_id");
                                        String user_name = jsonObject3.getString("user_name");
                                        String email = jsonObject3.getString("email");
                                        String mobile = jsonObject3.getString("mobile");
                                        final String account_status = jsonObject3.getString("account_status");
                                        String browser_session_id = jsonObject3.getString("browser_session_id");

                                        Log.d("LOGINUSERDETAIL:", jwt + "//" + user_id + "//" + user_name + "//" + email + "//" + mobile + "//" + account_status + "//" + browser_session_id);

                                        //session.createUserLoginSession(jwt, user_id, user_name, mobile, email, account_status, browser_session_id);
                                        if (getIntent().getExtras().getString("activity_name").equals("WelcomeActivity")) {
                                            Intent intent = new Intent(LoginPage.this, MainActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                            if (account_status.equals("0")) {
                                                Intent intse = new Intent(LoginPage.this, MainActivity.class);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                startActivity(intse);
                                            }
                                        } else {
                                            finish();
                                        }
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }

                                    Toast.makeText(getApplicationContext(), "You are Logged in Successfully...!", Toast.LENGTH_SHORT).show();

                                }

                                if (editSuccessResponceCode.equalsIgnoreCase("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Your account has been disabled...!", Toast.LENGTH_SHORT).show();
                                }
                                if (editSuccessResponceCode.equalsIgnoreCase("10300")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Login failed. Invalid username or password..!", Toast.LENGTH_SHORT).show();
                                }

                                if (editSuccessResponceCode.equalsIgnoreCase("11786")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "All fields are required..!", Toast.LENGTH_SHORT).show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {


                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("email", email);
                            params.put("password", psw);
                            Log.d("LoginREQUESTDATA:", params.toString());
                            return params;
                        }

                    };

                    strReqLogin.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    RequestQueue requestQueue = Volley.newRequestQueue(LoginPage.this);
                    requestQueue.add(strReqLogin);


                } else {
                    Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }

        }

        if (v == registerhere){

            Intent intent = new Intent(getApplicationContext(),RegisterPage.class);
            startActivity(intent);
        }

        if (v == login_forgotpassword){

            Intent intent = new Intent(getApplicationContext(),ForgotPasswordPage.class);
            startActivity(intent);
        }
    }

    private boolean validate() {

        boolean result = true;

        String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        String userName = username_txt.getText().toString().trim();
        String psw = password_txt.getText().toString().trim();

        if (userName.length() == 0) {
            username.setError("Please enter Email");
            result = false;
        } else if (!userName.matches(EMAIL_REGEX)) {
            username.setError("Invalid Email");
            result = false;
        } else if (psw.isEmpty() || psw.length() < 6) {
            password.setError("Invalid Password. Password Must contain minimum 6 Characters");
            result = false;
        } else if (!userName.matches(EMAIL_REGEX) && psw.isEmpty() || psw.length() < 6) {
            Toast.makeText(this, "Invalid Email and Password", Toast.LENGTH_SHORT).show();
            username.setError("Invalid Email and Password");
            result = false;
        }

        return result;
    }
}
