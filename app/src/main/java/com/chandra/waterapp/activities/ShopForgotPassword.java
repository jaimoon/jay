package com.chandra.waterapp.activities;

import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.chandra.waterapp.R;

public class ShopForgotPassword extends AppCompatActivity implements View.OnClickListener {

    ImageView app_logo;
    TextView forgot_txt;
    TextInputLayout forgot_email;
    EditText forgot_email_edt;
    Button forgot_cancel,send_otp;
    Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_forgot_password);

        typeface = Typeface.createFromAsset(this.getAssets(), "latoregular.ttf");

        app_logo = (ImageView) findViewById(R.id.app_logo);

        forgot_txt = (TextView) findViewById(R.id.forgot_txt);
        forgot_txt.setTypeface(typeface);

        forgot_email = (TextInputLayout) findViewById(R.id.forgot_email);
        forgot_email.setTypeface(typeface);

        forgot_email_edt = (EditText) findViewById(R.id.forgot_email_edt);
        forgot_email_edt.setTypeface(typeface);

        forgot_cancel = (Button) findViewById(R.id.forgot_cancel);
        forgot_cancel.setTypeface(typeface);
        forgot_cancel.setOnClickListener(this);
        send_otp = (Button) findViewById(R.id.send_otp);
        send_otp.setTypeface(typeface);
        send_otp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == forgot_cancel){

            finish();

        }

        if (v == send_otp){

        }

    }
}
