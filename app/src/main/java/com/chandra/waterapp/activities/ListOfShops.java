package com.chandra.waterapp.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.chandra.waterapp.R;


public class ListOfShops extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    Toolbar toolbar;
    ImageView close;
    TextView toobar_title;
    SearchView search_shops;
    private SwipeRefreshLayout shops_refresh;
    RecyclerView shops_recyclerview;
    Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_shops);

        typeface = Typeface.createFromAsset(this.getAssets(), "latoregular.ttf");

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);

        toobar_title = (TextView) findViewById(R.id.toobar_title);
        toobar_title.setTypeface(typeface);

        search_shops = (SearchView) findViewById(R.id.search_shops);
        EditText searchEditText = (EditText) search_shops.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTypeface(typeface);
        searchEditText.setTextColor(getResources().getColor(R.color.text_color));
        searchEditText.setHintTextColor(getResources().getColor(R.color.text_color));

        search_shops.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search_shops.setIconified(false);
            }
        });

        search_shops.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                //adapter.getFilter().filter(query);
                return false;
            }
        });

        shops_refresh = (SwipeRefreshLayout)findViewById(R.id.shops_refresh);
        shops_refresh.setColorSchemeResources(android.R.color.holo_blue_bright,

                android.R.color.holo_green_light,

                android.R.color.holo_orange_light,

                android.R.color.holo_red_light);

        shops_refresh.setOnRefreshListener(this);

        shops_recyclerview = (RecyclerView) findViewById(R.id.shops_recyclerview);

    }

    @Override
    public void onClick(View v) {

        if (v == close){
            Intent intent = new Intent(getApplicationContext(),DescriptionPage.class);
            startActivity(intent);
        }

    }

    @Override
    public void onRefresh() {

    }
}
