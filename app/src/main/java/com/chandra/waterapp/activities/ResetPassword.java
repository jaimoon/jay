package com.chandra.waterapp.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.chandra.waterapp.R;

public class ResetPassword extends AppCompatActivity implements View.OnClickListener {

    ImageView app_logo;
    TextView reset_password_txt;
    TextInputLayout current_password,reset_password;
    EditText current_password_edt,reset_password_edt;
    Button reset_cancel,reset_submit;
    Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        typeface = Typeface.createFromAsset(this.getAssets(), "latoregular.ttf");

        app_logo = (ImageView) findViewById(R.id.app_logo);

        reset_password_txt = (TextView) findViewById(R.id.reset_password_txt);
        reset_password_txt.setTypeface(typeface);

        current_password = (TextInputLayout) findViewById(R.id.current_password);
        current_password.setTypeface(typeface);
        reset_password = (TextInputLayout) findViewById(R.id.reset_password);
        reset_password.setTypeface(typeface);

        current_password_edt = (EditText) findViewById(R.id.current_password_edt);
        current_password_edt.setTypeface(typeface);
        reset_password_edt = (EditText) findViewById(R.id.reset_password_edt);
        reset_password_edt.setTypeface(typeface);

        reset_cancel = (Button) findViewById(R.id.reset_cancel);
        reset_cancel.setTypeface(typeface);
        reset_cancel.setOnClickListener(this);
        reset_submit = (Button) findViewById(R.id.reset_submit);
        reset_submit.setTypeface(typeface);
        reset_submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == reset_cancel){

            finish();

        }

        if (v == reset_submit){

        }

    }
}
