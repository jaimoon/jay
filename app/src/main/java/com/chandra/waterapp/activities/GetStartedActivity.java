package com.chandra.waterapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.chandra.waterapp.R;

public class GetStartedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_started);
    }
}
