package com.chandra.waterapp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chandra.waterapp.MainActivity;
import com.chandra.waterapp.R;
import com.chandra.waterapp.utilities.AppUrls;
import com.chandra.waterapp.utilities.NetworkChecking;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class AccountVerificationActivity extends AppCompatActivity implements View.OnClickListener {

    TextInputLayout otp_til;
    EditText otp_edt;
    Button resend_txt,submit_btn;
    Typeface typeface;
    ProgressDialog progressDialog;
    boolean checkInternet;
    String mobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_verification);

        typeface = Typeface.createFromAsset(this.getAssets(), "latoregular.ttf");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        mobile = getIntent().getExtras().getString("activity_name");

        otp_til = findViewById(R.id.otp_til);
        otp_til.setTypeface(typeface);
        otp_edt = findViewById(R.id.otp_edt);
        otp_edt.setTypeface(typeface);
        resend_txt = findViewById(R.id.resend_txt);
        resend_txt.setTypeface(typeface);
        resend_txt.setOnClickListener(this);
        submit_btn = findViewById(R.id.submit_btn);
        submit_btn.setTypeface(typeface);
        submit_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == resend_txt){
            checkInternet = NetworkChecking.isConnected(this);

            if (checkInternet) {
                progressDialog.show();
                Log.d("RESENDURL:", AppUrls.BASE_URL + AppUrls.RESENDOTP);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.RESENDOTP,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("RESENDOTPRESP:", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String successResponceCode = jsonObject.getString("status");
                                    if (successResponceCode.equals("10100 ")) {
                                        progressDialog.dismiss();

                                        Toast.makeText(getApplicationContext(), "New OTP Sent Successfully", Toast.LENGTH_SHORT).show();

                                    }
                                    if (successResponceCode.equals("10200")) {
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "Sorry, Try again...!", Toast.LENGTH_SHORT).show();
                                    }

                                    if (successResponceCode.equals("10300")) {
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "Incorrect Mobile No....!", Toast.LENGTH_SHORT).show();
                                    }
                                    if (successResponceCode.equals("11786")) {
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "All fields are required...!", Toast.LENGTH_SHORT).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    progressDialog.cancel();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                progressDialog.cancel();

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Mobile", mobile);
                        Log.d("RESENDOTPPARAM:", params.toString());
                        return params;
                    }
                };
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(AccountVerificationActivity.this);
                requestQueue.add(stringRequest);

            } else {
                Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == submit_btn){

            if (validate()) {
                checkInternet = NetworkChecking.isConnected(this);
                if (checkInternet) {
                    final String otp = otp_edt.getText().toString().trim();
                    progressDialog.show();
                    Log.d("ACCOUNTURL", AppUrls.BASE_URL + AppUrls.VERIFYACCOUNT);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.VERIFYACCOUNT,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("ACCOUNTVERIRESP", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String successResponceCode = jsonObject.getString("status");
                                        if (successResponceCode.equals("10200")) {

                                            progressDialog.dismiss();

                                            Intent intent = new Intent(AccountVerificationActivity.this, MainActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);

                                        }
                                        if (successResponceCode.equals("10100")) {
                                            progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Sorry, Try again...!", Toast.LENGTH_SHORT).show();
                                        }
                                        if (successResponceCode.equals("10300 ")) {
                                            progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Incorrect OTP...!", Toast.LENGTH_SHORT).show();
                                        }
                                        if (successResponceCode.equals("11786")) {
                                            progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "All fields are required...!", Toast.LENGTH_SHORT).show();
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                    } else if (error instanceof AuthFailureError) {

                                    } else if (error instanceof ServerError) {

                                    } else if (error instanceof NetworkError) {

                                    } else if (error instanceof ParseError) {

                                    }
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("otp", otp);
                            params.put("mobile", mobile);
                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(AccountVerificationActivity.this);
                    requestQueue.add(stringRequest);

                } else {
                    Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        }
    }

    private boolean validate() {

        boolean result = true;
        String otp = otp_edt.getText().toString().trim();
        if ((otp == null || otp.equals("")) || otp.length() != 6) {
            otp_til.setError("Invalid OTP");
            result = false;
        } else {
            otp_til.setErrorEnabled(false);
        }

        return result;
    }
}
