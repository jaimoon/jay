package com.chandra.waterapp.activities;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.chandra.waterapp.R;


public class DescriptionPage extends AppCompatActivity implements View.OnClickListener {

    ImageView close;
    TextView toobar_title,items_count,amount,item_count_txt,amount_txt;
    Button pay_btn,dummy_btn;
    RecyclerView address_recyclerview;
    AlertDialog dialog;
    Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description_page);

        typeface = Typeface.createFromAsset(this.getAssets(), "latoregular.ttf");

        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);

        toobar_title = (TextView) findViewById(R.id.toobar_title);
        toobar_title.setTypeface(typeface);
        items_count = (TextView) findViewById(R.id.items_count);
        items_count.setTypeface(typeface);
        amount = (TextView) findViewById(R.id.amount);
        amount.setTypeface(typeface);
        item_count_txt = (TextView) findViewById(R.id.item_count_txt);
        item_count_txt.setTypeface(typeface);
        amount_txt = (TextView) findViewById(R.id.amount_txt);
        amount_txt.setTypeface(typeface);

        address_recyclerview = (RecyclerView) findViewById(R.id.address_recyclerview);

        pay_btn = (Button) findViewById(R.id.pay_btn);
        pay_btn.setTypeface(typeface);
        pay_btn.setOnClickListener(this);

        dummy_btn = (Button) findViewById(R.id.dummy_btn);
        dummy_btn.setTypeface(typeface);
        dummy_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == close){
            finish();
        }
        if (v == pay_btn){

            AlertDialog.Builder builder = new AlertDialog.Builder(DescriptionPage.this);
            LayoutInflater inflater = getLayoutInflater();

            View dialog_layout = inflater.inflate(R.layout.custom_dialog_payment_selection, null);
            TextView dialog_title = (TextView) dialog_layout.findViewById(R.id.dialog_title);
            dialog_title.setTypeface(typeface);
            Button online_btn = (Button) dialog_layout.findViewById(R.id.online_btn);
            online_btn.setTypeface(typeface);
            online_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();

                }
            });
            Button cod_btn = (Button) dialog_layout.findViewById(R.id.cod_btn);
            cod_btn.setTypeface(typeface);
            cod_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                }
            });
            builder.setView(dialog_layout)
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });

            dialog = builder.create();
            dialog.show();
        }else {
            //showSnackMessage("NO INTERNET CONNECTION");
        }

        if (v == dummy_btn){

            AlertDialog.Builder builder = new AlertDialog.Builder(DescriptionPage.this);
            LayoutInflater inflater = getLayoutInflater();

            View dialog_layout = inflater.inflate(R.layout.custom_daialog_address_form, null);

            TextView form_title = (TextView) dialog_layout.findViewById(R.id.form_title);
            form_title.setTypeface(typeface);

            EditText name_edt = (EditText) dialog_layout.findViewById(R.id.name_edt);
            name_edt.setTypeface(typeface);

            EditText phone_edt = (EditText) dialog_layout.findViewById(R.id.phone_edt);
            phone_edt.setTypeface(typeface);

            EditText plot_number_edt = (EditText) dialog_layout.findViewById(R.id.plot_number_edt);
            plot_number_edt.setTypeface(typeface);

            EditText street_name_edt = (EditText) dialog_layout.findViewById(R.id.street_name_edt);
            street_name_edt.setTypeface(typeface);

            EditText area_name_edt = (EditText) dialog_layout.findViewById(R.id.area_name_edt);
            area_name_edt.setTypeface(typeface);

            EditText city_name_edt = (EditText) dialog_layout.findViewById(R.id.city_name_edt);
            city_name_edt.setTypeface(typeface);

            EditText state_name_edt = (EditText) dialog_layout.findViewById(R.id.state_name_edt);
            state_name_edt.setTypeface(typeface);

            EditText pin_number_edt = (EditText) dialog_layout.findViewById(R.id.pin_number_edt);
            pin_number_edt.setTypeface(typeface);

            Button cancel_dialog = (Button) dialog_layout.findViewById(R.id.cancel_dialog);
            cancel_dialog.setTypeface(typeface);
            cancel_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();

                }
            });

            Button submit_btn = (Button) dialog_layout.findViewById(R.id.submit_btn);
            submit_btn.setTypeface(typeface);
            submit_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                }
            });

            builder.setView(dialog_layout);/*.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });*/

            dialog = builder.create();
            dialog.show();

        }else {
            //showSnackMessage("NO INTERNET CONNECTION");
        }

    }
}
