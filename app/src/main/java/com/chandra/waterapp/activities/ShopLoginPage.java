package com.chandra.waterapp.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.chandra.waterapp.MainActivity;
import com.chandra.waterapp.R;

public class ShopLoginPage extends AppCompatActivity implements View.OnClickListener {

    ImageView login_logo;
    TextView login_txt,login_forgotpassword,login_donthaveaccount,registerhere;
    TextInputLayout username,password;
    EditText username_txt;
    TextInputEditText password_txt;
    Button login_cancel,login_submit;
    Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_login_page);

        typeface = Typeface.createFromAsset(this.getAssets(), "latoregular.ttf");

        login_logo = (ImageView) findViewById(R.id.login_logo);

        login_txt = (TextView) findViewById(R.id.login_txt);
        login_txt.setTypeface(typeface);
        login_forgotpassword = (TextView) findViewById(R.id.login_forgotpassword);
        login_forgotpassword.setTypeface(typeface);
        login_forgotpassword.setOnClickListener(this);
        login_donthaveaccount = (TextView) findViewById(R.id.login_donthaveaccount);
        login_donthaveaccount.setTypeface(typeface);
        registerhere = (TextView) findViewById(R.id.registerhere);
        registerhere.setTypeface(typeface);
        registerhere.setOnClickListener(this);

        username_txt = (EditText) findViewById(R.id.username_txt);
        username_txt.setTypeface(typeface);

        username = (TextInputLayout) findViewById(R.id.username);
        username.setTypeface(typeface);
        password = (TextInputLayout) findViewById(R.id.password);
        password.setTypeface(typeface);

        password_txt = (TextInputEditText) findViewById(R.id.password_txt);
        password_txt.setTypeface(typeface);

        login_cancel = (Button) findViewById(R.id.login_cancel);
        login_cancel.setTypeface(typeface);
        login_cancel.setOnClickListener(this);

        login_submit = (Button) findViewById(R.id.login_submit);
        login_submit.setTypeface(typeface);
        login_submit.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if (v == login_cancel){

            finish();

        }

        if (v == login_submit){

            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);

        }

        if (v == registerhere){

            Intent intent = new Intent(getApplicationContext(),ShopRegistrationPage.class);
            startActivity(intent);
        }

        if (v == login_forgotpassword){

            Intent intent = new Intent(getApplicationContext(),ShopRegistrationPage.class);
            startActivity(intent);
        }

    }
}
