package com.chandra.waterapp.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.chandra.waterapp.R;


public class WelcomePage extends AppCompatActivity implements View.OnClickListener {

    ImageView app_logo;
    TextView welcome_tittle,welcome_txt;
    Button shopkeeper_btn,customer_btn;
    Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_page);

        typeface = Typeface.createFromAsset(this.getAssets(), "latoregular.ttf");

        app_logo = (ImageView) findViewById(R.id.app_logo);

        welcome_tittle = (TextView) findViewById(R.id.welcome_tittle);
        welcome_tittle.setTypeface(typeface);
        welcome_txt = (TextView) findViewById(R.id.welcome_txt);
        welcome_txt.setTypeface(typeface);

        shopkeeper_btn = (Button) findViewById(R.id.shopkeeper_btn);
        shopkeeper_btn.setTypeface(typeface);
        shopkeeper_btn.setOnClickListener(this);
        customer_btn = (Button) findViewById(R.id.customer_btn);
        customer_btn.setTypeface(typeface);
        customer_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == shopkeeper_btn){

            Intent intent = new Intent(getApplicationContext(),ShopLoginPage.class);
            startActivity(intent);
        }

        if (v == customer_btn){

            Intent intent = new Intent(getApplicationContext(),LoginPage.class);
            startActivity(intent);
        }

    }
}
