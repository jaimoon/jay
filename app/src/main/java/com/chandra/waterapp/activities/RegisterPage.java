package com.chandra.waterapp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chandra.waterapp.R;
import com.chandra.waterapp.utilities.AppUrls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class RegisterPage extends AppCompatActivity implements View.OnClickListener {

    ImageView reg_logo;
    TextView reg_txt, or_txt, reg_haveaccount, loginhere;
    Button google_plus, facebook, reg_cancel, reg_submit;
    TextInputLayout username, email, phone, password;
    TextInputEditText password_txt;
    EditText username_txt, email_txt, phone_txt;
    ProgressDialog progressDialog;
    Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_page);

        typeface = Typeface.createFromAsset(this.getAssets(), "latoregular.ttf");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        reg_logo = (ImageView) findViewById(R.id.reg_logo);

        reg_txt = (TextView) findViewById(R.id.reg_txt);
        reg_txt.setTypeface(typeface);
        or_txt = (TextView) findViewById(R.id.or_txt);
        or_txt.setTypeface(typeface);
        reg_haveaccount = (TextView) findViewById(R.id.reg_haveaccount);
        reg_haveaccount.setTypeface(typeface);
        loginhere = (TextView) findViewById(R.id.loginhere);
        loginhere.setTypeface(typeface);
        loginhere.setOnClickListener(this);

        username = (TextInputLayout) findViewById(R.id.username);
        username.setTypeface(typeface);
        email = (TextInputLayout) findViewById(R.id.email);
        email.setTypeface(typeface);
        phone = (TextInputLayout) findViewById(R.id.phone);
        phone.setTypeface(typeface);
        password = (TextInputLayout) findViewById(R.id.password);
        password.setTypeface(typeface);

        password_txt = (TextInputEditText) findViewById(R.id.password_txt);
        password_txt.setTypeface(typeface);

        username_txt = (EditText) findViewById(R.id.username_txt);
        username_txt.setTypeface(typeface);
        email_txt = (EditText) findViewById(R.id.email_txt);
        email_txt.setTypeface(typeface);
        phone_txt = (EditText) findViewById(R.id.phone_txt);
        phone_txt.setTypeface(typeface);

        google_plus = (Button) findViewById(R.id.google_plus);
        google_plus.setTypeface(typeface);
        google_plus.setOnClickListener(this);
        facebook = (Button) findViewById(R.id.facebook);
        facebook.setTypeface(typeface);
        facebook.setOnClickListener(this);
        reg_cancel = (Button) findViewById(R.id.reg_cancel);
        reg_cancel.setTypeface(typeface);
        reg_cancel.setOnClickListener(this);
        reg_submit = (Button) findViewById(R.id.reg_submit);
        reg_submit.setTypeface(typeface);
        reg_submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {


        if (v == loginhere) {

            Intent intent = new Intent(getApplicationContext(), LoginPage.class);
            startActivity(intent);

        }

        if (v == google_plus) {

        }

        if (v == facebook) {

        }

        if (v == reg_cancel) {

            finish();

        }

        if (v == reg_submit) {

            register();

        }

    }

    private void register() {

        final String name = username_txt.getText().toString().trim();
        final String mobile = phone_txt.getText().toString().trim();
        final String email = email_txt.getText().toString().trim();
        final String password = password_txt.getText().toString().trim();

        PackageInfo pInfo = null;
        String version = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        final String finalVersion = version;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.REGISTER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.d("RESPONCELREG", response);
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            String editSuccessResponceCode = jsonObject.getString("status");
                            if (editSuccessResponceCode.equalsIgnoreCase("10100")) {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Send OTP to your mobile....! ", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(RegisterPage.this, AccountVerificationActivity.class);
                                intent.putExtra("activity_name", getIntent().getExtras().getString("activity_name"));
                                intent.putExtra("mobile", mobile);
                                startActivity(intent);
                            }
                            if (editSuccessResponceCode.equals("10400")) {
                                Toast.makeText(getApplicationContext(), "Email already exits", Toast.LENGTH_SHORT).show();
                            }
                            if (editSuccessResponceCode.equals("10300")) {
                                Toast.makeText(getApplicationContext(), "Mobile already exits", Toast.LENGTH_SHORT).show();
                            }
                            if (editSuccessResponceCode.equals("10200")) {
                                Toast.makeText(getApplicationContext(), "Sorry, Try again....!", Toast.LENGTH_SHORT).show();
                            }
                            if (editSuccessResponceCode.equals("11786")) {
                                Toast.makeText(getApplicationContext(), "All fields are required....!", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", name);
                params.put("email", email);
                params.put("mobile", mobile);
                params.put("conf_pwd", password);
                Log.d("RegisterREQUESTDATA:", params.toString());
                return params;
            }
                       @Override
                       public byte[] getBody() throws AuthFailureError
                       {
                           Map<String, String> params = new HashMap<String, String>();
                           params.put("name", name);
                           params.put("email", email);
                           params.put("mobile", mobile);
                           params.put("password", password);

                           Log.d("RegisterREQUESTDATA ", params.toString());
                           return new JSONObject(params).toString().getBytes();
                       }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(RegisterPage.this);
        requestQueue.add(stringRequest);


    }
}
