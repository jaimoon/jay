package com.chandra.waterapp.activities;

import android.graphics.Typeface;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.chandra.waterapp.R;

public class ShopRegistrationPage extends AppCompatActivity implements View.OnClickListener {

    ImageView app_logo;
    TextView shop_reg_txt,display_text,monday_time,tuesday_time,wednesday_time,thursday_time,friday_time,saturday_time,sunday_time;
    TextInputLayout username,email,phone,password,count;
    EditText username_edt,email_edt,phone_edt,count_edt;
    TextInputEditText password_txt;
    Button pick_address,shop_reg_cancel,shop_reg_submit;
    CheckBox ck_mon,ck_tus,ck_wed,ck_thu,ck_fri,ck_sat,ck_sun,cod_checkbox,online_checkbox,telebook_checkbox;
    Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_registration_page);

        typeface = Typeface.createFromAsset(this.getAssets(), "latoregular.ttf");

        app_logo = (ImageView) findViewById(R.id.app_logo);

        shop_reg_txt = (TextView) findViewById(R.id.shop_reg_txt);
        shop_reg_txt.setTypeface(typeface);
        display_text = (TextView) findViewById(R.id.display_text);
        display_text.setTypeface(typeface);
        monday_time = (TextView) findViewById(R.id.monday_time);
        monday_time.setTypeface(typeface);
        tuesday_time = (TextView) findViewById(R.id.tuesday_time);
        tuesday_time.setTypeface(typeface);
        wednesday_time = (TextView) findViewById(R.id.wednesday_time);
        wednesday_time.setTypeface(typeface);
        thursday_time = (TextView) findViewById(R.id.thursday_time);
        thursday_time.setTypeface(typeface);
        friday_time = (TextView) findViewById(R.id.friday_time);
        friday_time.setTypeface(typeface);
        saturday_time = (TextView) findViewById(R.id.saturday_time);
        saturday_time.setTypeface(typeface);
        sunday_time = (TextView) findViewById(R.id.sunday_time);
        sunday_time.setTypeface(typeface);

        username = (TextInputLayout) findViewById(R.id.username);
        username.setTypeface(typeface);
        email = (TextInputLayout) findViewById(R.id.email);
        email.setTypeface(typeface);
        phone = (TextInputLayout) findViewById(R.id.phone);
        phone.setTypeface(typeface);
        password = (TextInputLayout) findViewById(R.id.password);
        password.setTypeface(typeface);
        count = (TextInputLayout) findViewById(R.id.count);
        count.setTypeface(typeface);

        username_edt = (EditText) findViewById(R.id.username_edt);
        username_edt.setTypeface(typeface);
        email_edt = (EditText) findViewById(R.id.email_edt);
        email_edt.setTypeface(typeface);
        phone_edt = (EditText) findViewById(R.id.phone_edt);
        phone_edt.setTypeface(typeface);
        count_edt = (EditText) findViewById(R.id.count_edt);
        count_edt.setTypeface(typeface);

        password_txt = (TextInputEditText) findViewById(R.id.password_txt);
        password_txt.setTypeface(typeface);

        pick_address = (Button) findViewById(R.id.pick_address);
        pick_address.setTypeface(typeface);
        pick_address.setOnClickListener(this);
        shop_reg_cancel = (Button) findViewById(R.id.shop_reg_cancel);
        shop_reg_cancel.setTypeface(typeface);
        shop_reg_cancel.setOnClickListener(this);
        shop_reg_submit = (Button) findViewById(R.id.shop_reg_submit);
        shop_reg_submit.setTypeface(typeface);
        shop_reg_submit.setOnClickListener(this);

        ck_mon = (CheckBox) findViewById(R.id.ck_mon);
        ck_mon.setTypeface(typeface);
        ck_tus = (CheckBox) findViewById(R.id.ck_tus);
        ck_tus.setTypeface(typeface);
        ck_wed = (CheckBox) findViewById(R.id.ck_wed);
        ck_wed.setTypeface(typeface);
        ck_thu = (CheckBox) findViewById(R.id.ck_thu);
        ck_thu.setTypeface(typeface);
        ck_fri = (CheckBox) findViewById(R.id.ck_fri);
        ck_fri.setTypeface(typeface);
        ck_sat = (CheckBox) findViewById(R.id.ck_sat);
        ck_sat.setTypeface(typeface);
        ck_sun = (CheckBox) findViewById(R.id.ck_sun);
        ck_sun.setTypeface(typeface);
        cod_checkbox = (CheckBox) findViewById(R.id.cod_checkbox);
        cod_checkbox.setTypeface(typeface);
        online_checkbox = (CheckBox) findViewById(R.id.online_checkbox);
        online_checkbox.setTypeface(typeface);
        telebook_checkbox = (CheckBox) findViewById(R.id.telebook_checkbox);
        telebook_checkbox.setTypeface(typeface);
    }

    @Override
    public void onClick(View v) {

        if (v == shop_reg_cancel){
            finish();
        }

    }
}
